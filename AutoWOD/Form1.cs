﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoWOD
{

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        
        int athleteCode = 0;
        string athleteGender = "";
        string showStr = "";
        string[] ex = new string[10];
        Random rand = new Random();

        List<string> schemesAFAP = new List<string>();
        List<string> schemesAMRAP = new List<string>();
        List<string> exercises = new List<string>();
        string previousExType = "";
        string currentScheme = "";
        string weight = "";
        string savedWod = "";
        int[] schemeValues = new int[100];
        int wodtype = 0;
        int speed = 0;
        int sumOfReps = 0;
        double exCount = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
            wodDescrTextBox.SelectionAlignment = HorizontalAlignment.Center;
            workoutTextBox.SelectionAlignment = HorizontalAlignment.Center;
            tabSelection();
            getNeedList();

            schemesAFAP.Add("21-15-9");
            schemesAFAP.Add("42-30-18");
            schemesAFAP.Add("30-20-10");
            schemesAFAP.Add("10-20-30");
            schemesAFAP.Add("9-15-21");
            schemesAFAP.Add("18-30-42");
            schemesAFAP.Add("50-40-30-20-10");
            schemesAFAP.Add("10-20-30-40-50");
            schemesAFAP.Add("27-21-18-15-12-9-6-3");
            schemesAFAP.Add("3-6-9-12-15-18-21-27");
            schemesAFAP.Add("1-2-3-4-5");
            schemesAFAP.Add("1-2-3-4-5-6-7-8-9-10");
            schemesAFAP.Add("10-9-8-7-6-5-4-3-2-1-1");

            schemesAMRAP.Add("21-15-9");
            schemesAMRAP.Add("42-30-18");
            schemesAMRAP.Add("30-20-10");
            schemesAMRAP.Add("10-20-30");
            schemesAMRAP.Add("9-15-21");
            schemesAMRAP.Add("18-30-42");
            schemesAMRAP.Add("50-40-30-20-10");
            schemesAMRAP.Add("10-20-30-40-50");
            schemesAMRAP.Add("90-60-30");
            schemesAMRAP.Add("30-60-90");
            schemesAMRAP.Add("1-2-3-...");
            schemesAMRAP.Add("2-4-6-...");
            schemesAMRAP.Add("3-6-9-...");
        }
        private void tabSelection()
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet1 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet2 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet3 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet4 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet5 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet6 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet7 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet8 = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter1 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter2 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter3 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter4 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter5 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter6 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter7 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter8 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;
            dataSetAdapter1.SelectCommand = command;
            dataSetAdapter2.SelectCommand = command;
            dataSetAdapter3.SelectCommand = command;
            dataSetAdapter4.SelectCommand = command;
            dataSetAdapter5.SelectCommand = command;
            dataSetAdapter6.SelectCommand = command;
            dataSetAdapter7.SelectCommand = command;
            dataSetAdapter8.SelectCommand = command;

            //////////////////////////////////////////////////////1
            command.CommandText = "SELECT * FROM [Атлет]";
            dataSetAdapter.Fill(dataSet);
            athleteDataDGV.DataSource = dataSet.Tables[0];
            chooseAthlete();

            //////////////////////////////////////////////////////2
            command.CommandText = "SELECT * FROM [Показатели]";
            dataSetAdapter1.Fill(dataSet1);
            listDGV.DataSource = dataSet1.Tables[0];
            //////////////////////////////////////////////////////3
            command.CommandText = "SELECT [Наименование], [Норматив], [Начальный], [Средний], [Продвинутый], [Элитный] FROM [Нормативы] ORDER BY [Норматив] DESC, [Наименование] ASC";
            dataSetAdapter2.Fill(dataSet2);
            standardsDGV.DataSource = dataSet2.Tables[0];

            //////////////////////////////////////////////////////4
            command.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                "INNER JOIN (SELECT * FROM [Результаты] WHERE [Код атлета] = " + athleteCode + ") res ON [Атлет].[Код атлета] = res.[Код атлета]";
            dataSetAdapter3.Fill(dataSet3);
            resultsDGV.DataSource = dataSet3.Tables[0];
            
            resultsDGV.Columns["Наименование"].ReadOnly = true;
            resultsDGV.Columns["Уровень атлета"].ReadOnly = true;

            command.CommandText = "SELECT * FROM [Таблица масштабирования]";
            dataSetAdapter5.Fill(dataSet5);
            scalingDGV.DataSource = dataSet5.Tables[0];

            command.CommandText = "SELECT [Бенчмарк], [Комбинированное], [Толкательное], [Тяговое], [Ноги], [Пресс], [Итоговый уровень] FROM [Уровень в движениях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter6.Fill(dataSet6);
            levelsDGV.DataSource = dataSet6.Tables[0];

            command.CommandText = "SELECT [G], [W], [M], [Итоговый уровень] FROM [Уровень в модальностях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter7.Fill(dataSet7);
            modalitiesDGV.DataSource = dataSet7.Tables[0];

            command.CommandText = "SELECT [Код записи], [Описание комплекса], [Результат], [Примечания], [Дата выполнения] FROM [Комплекс] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter8.Fill(dataSet8);
            doneWodsDGV.DataSource = dataSet8.Tables[0];

            connection.Close();
            
        }
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void generateButton_Click(object sender, EventArgs e)
        {

                exercises.Clear();
                previousExType = "";

                workoutTextBox.Text = "";

                
                speed = rand.Next(1, 4);
                exCount = rand.Next(2, 4);
                wodtype = rand.Next(0, 1);

            
            switch (wodtype)
            {

                case 0:
                    workoutTextBox.Text += "На время:\n\n";
                    //workoutTextBox.Text += currentScheme + "\n";
                    break;
                case 1:
                    workoutTextBox.Text += "Как можно больше\nповторений за " + rand.Next(3, 9) + " минут:\n\n";
                    //workoutTextBox.Text += currentScheme + "\n";
                    break;

            }
            if (charRB1.Checked == true)
            {

                speed = rand.Next(1, 6);

                switch (speed)
                {

                    case 1:
                        workoutTextBox.Text += "(спринт)" + "\n\n";
                        break;
                    case 2:
                        workoutTextBox.Text += "(быстрый)" + "\n\n";
                        break;
                    case 3:
                        workoutTextBox.Text += "(умеренный)" + "\n\n";
                        break;
                    case 4:
                        workoutTextBox.Text += "(медленный)" + "\n\n";
                        break;
                    case 5:
                        workoutTextBox.Text += "(очень медленный)" + "\n\n";
                        break;

                }
            }
            else if (charRB2.Checked == true)
            {

                speed = 1;
                workoutTextBox.Text += "(спринт)" + "\n\n";

            }
            else if (charRB3.Checked == true)
            {

                speed = 2;
                workoutTextBox.Text += "(быстрый)" + "\n\n";

            }
            else if (charRB4.Checked == true)
            {

                speed = 3;
                workoutTextBox.Text += "(умеренный)" + "\n\n";

            }
            else if (charRB5.Checked == true)
            {

                speed = 4;
                workoutTextBox.Text += "(медленный)" + "\n\n";

            }
            else if (charRB6.Checked == true)
            {

                speed = 5;
                workoutTextBox.Text += "(очень медленный)" + "\n\n";

            }


                                        while (true)
                                        {

                                                            sumOfReps = 0;
                                                            wodtype = rand.Next(0, 1);
                                                            string[] Reps1 = new string[100];
                                                            switch (wodtype)
                                                            {

                                                                case 0:
                                                                    currentScheme = schemesAFAP[rand.Next(0, (schemesAFAP.Count - 1))];
                                                                    break;
                                                                case 1:
                                                                    currentScheme = schemesAMRAP[rand.Next(0, (schemesAMRAP.Count - 1))];
                                                                    break;

                                                            }
                                                            Reps1 = currentScheme.Split('-');
                                                            for (int i = 0; i < Reps1.Length; i++)
                                                                sumOfReps += Convert.ToInt32(Reps1[i]);//подсчёт суммы повторений в схеме

                                                            if (sumOfReps <= (speed * 60)) break;
                                        }
            for (int i = 0; i < exCount; i++)
            {
                generateExercise(); // генерация упражнения в комплексе

            }

            
        }

        private void athleteDataDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            chooseAthlete();
        }
        private void chooseAthlete()
        {
            athleteCode = Convert.ToInt32(athleteDataDGV.CurrentRow.Cells[0].Value);
            athleteGender = athleteDataDGV.CurrentRow.Cells[4].Value.ToString();
            showName();

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet1 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet2 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet3 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet4 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet5 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet6 = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter1 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter2 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter3 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter4 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter5 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter6 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;
            dataSetAdapter1.SelectCommand = command;
            dataSetAdapter2.SelectCommand = command;
            dataSetAdapter3.SelectCommand = command;
            dataSetAdapter4.SelectCommand = command;
            dataSetAdapter5.SelectCommand = command;
            dataSetAdapter6.SelectCommand = command;

            command.CommandText = "SELECT [Бенчмарк], [Комбинированное], [Толкательное], [Тяговое], [Ноги], [Пресс], [Итоговый уровень] FROM [Уровень в движениях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter.Fill(dataSet);
            levelsDGV.DataSource = dataSet.Tables[0];

            command.CommandText = "SELECT [G], [W], [M], [Итоговый уровень] FROM [Уровень в модальностях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter5.Fill(dataSet5);
            modalitiesDGV.DataSource = dataSet5.Tables[0];

            command.CommandText = "SELECT * FROM [Показатели]";
            dataSetAdapter1.Fill(dataSet1);
            listDGV.DataSource = dataSet1.Tables[0];
            //////////////////////////////////////////////////////3
            command.CommandText = "SELECT [Наименование], [Норматив], [Начальный], [Средний], [Продвинутый], [Элитный] FROM [Нормативы] ORDER BY [Норматив] DESC, [Наименование] ASC";
            dataSetAdapter2.Fill(dataSet2);
            standardsDGV.DataSource = dataSet2.Tables[0];

            //////////////////////////////////////////////////////4
            command.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                "INNER JOIN (SELECT * FROM [Результаты] WHERE [Код атлета] = " + athleteCode + ") res ON [Атлет].[Код атлета] = res.[Код атлета]";
            dataSetAdapter3.Fill(dataSet3);
            resultsDGV.DataSource = dataSet3.Tables[0];
            resultsDGV.Columns["Наименование"].ReadOnly = true;
            resultsDGV.Columns["Уровень атлета"].ReadOnly = true;

                command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
                "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
                ") res ON[Показатели].[Наименование] = res.[Наименование] " +
                "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех')" +
                " ORDER BY [Показатели].[Наименование] ASC";
                dataSetAdapter4.Fill(dataSet4);
                countingDGV.DataSource = dataSet4.Tables[0];//вызов таблицы генерации, в которой находятся упражнения только сгенерированного упражнения


                resultComboBox.Items.Clear();
                for (int i = 0; i < countingDGV.RowCount; i++) resultComboBox.Items.Add(countingDGV.Rows[i].Cells[0].Value.ToString());

            command.CommandText = "SELECT [Код записи], [Описание комплекса], [Результат], [Примечания], [Дата выполнения] FROM [Комплекс] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter6.Fill(dataSet6);
            doneWodsDGV.DataSource = dataSet6.Tables[0];

            connection.Close();

            getNeedList();

            getTypeCountingTable("Бенчмарк");
            getTypeCountingTable("Комбинированное");
            getTypeCountingTable("Толкательное");
            getTypeCountingTable("Тяговое");
            getTypeCountingTable("Ноги");
            getTypeCountingTable("Пресс");
            getTypeCountingTable("Итоговый уровень");

            getModalityCountingTable("G");
            getModalityCountingTable("W");
            getModalityCountingTable("M");
            getModalityCountingTable("Итоговый уровень");

            chart1.Series["Комплексы"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[0].Value.ToString()));
            chart1.Series["Комбинированное"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[1].Value.ToString()));
            chart1.Series["Толкательное"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[2].Value.ToString()));
            chart1.Series["Тяговое"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[3].Value.ToString()));
            chart1.Series["Ноги"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[4].Value.ToString()));
            chart1.Series["Пресс"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[5].Value.ToString()));
            chart2.Series["G"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[0].Value.ToString()));
            chart2.Series["W"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[1].Value.ToString()));
            chart2.Series["M"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[2].Value.ToString()));

            chart1.ChartAreas[0].RecalculateAxesScale();
            chart2.ChartAreas[0].RecalculateAxesScale();

            chart1.Refresh();
            chart2.Refresh();

            

        }
        private void getMoreWodInfo()
        {
            bool exit = false;
            showStr = listDGV.CurrentRow.Cells[0].Value.ToString();
            for (int i = 0; i < standardsDGV.RowCount; i++)
            {
                if(
                    standardsDGV.Rows[i].Cells[0].Value.ToString() == showStr 
                    && 
                    (standardsDGV.Rows[i].Cells[1].Value.ToString() == athleteGender || standardsDGV.Rows[i].Cells[1].Value.ToString() == "Для всех")
                )
                {
                    for (int j = 0; j < listDGV.RowCount; j++)
                    {
                        if (showStr == listDGV.CurrentRow.Cells[0].Value.ToString())
                        {
                            

                            wodDescrTextBox.Text +=
                            "Уровни:" +
                            "\nНачальный - < " + standardsDGV.Rows[i].Cells[2].Value.ToString() +
                            "\nСредний - < " + standardsDGV.Rows[i].Cells[3].Value.ToString() +
                            "\nПродвинутый - < " + standardsDGV.Rows[i].Cells[4].Value.ToString() +
                            "\nЭлитный - < " + standardsDGV.Rows[i].Cells[5].Value.ToString();
                            
                            exit = true;
                            break;
                        }
                    }
                    if (exit) break;
                }
            }

        }

        private void showName()
        {

            label5.Text = athleteDataDGV.CurrentRow.Cells[1].Value.ToString() + " " +
                athleteDataDGV.CurrentRow.Cells[2].Value.ToString() + " " +
                athleteDataDGV.CurrentRow.Cells[3].Value.ToString();

        }

        private void mainTabControl_Selected(object sender, TabControlEventArgs e)
        {
            //if (athleteCode == 0 && mainTabControl.SelectedTab.Text != "Выбор атлета")
            //{
            //    MessageBox.Show("Сначала необходимо выбрать атлета.");
            //    mainTabControl.SelectTab(athleteData);
            //}
            if(listBox1.Items.Count != 0 && mainTabControl.SelectedTab.Text == "Комплекс")
            {
                MessageBox.Show("Не все необходимые показатели записаны для атлета.");
                mainTabControl.SelectTab(results);
            }
        }

        private void saveButton6_Click(object sender, EventArgs e)
        {
            savedWod = "";

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            string result = "";
            string beginner = "";
            string medium = "";
            string good = "";
            string elite = "";

            string level = "";

            double r = 0, b = 0, m = 0, g = 0, el = 0;

            for (int i = 0; i < resultsDGV.RowCount; i++)
            {
                            try
                            {
                                    if (Convert.ToDateTime(resultsDGV.Rows[i].Cells[3].Value.ToString()) > DateTime.Now)
                                    {
                                        MessageBox.Show("Дата не может быть позднее сегодняшней, строка №" +
                                            (i + 1), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    if (resultsDGV.Rows[i].Cells[1].Value.ToString() == "")
                                    {
                                        resultsDGV.Rows[i].Cells[2].Value = "";
                                        continue;
                                    }
                                    result = resultsDGV.Rows[i].Cells[1].Value.ToString();

                            
                                    if (result.Length > 2 && result[2] == ':')
                                    {
                                        if
                                            (

                                                result.Length != 5
                                                &&
                                                Convert.ToInt32(result[3]) > 5
                                            )
                                        {
                                            throw new FormatException();
                                        }

                                        Convert.ToInt32(result[0]);
                                        Convert.ToInt32(result[1]);
                                        Convert.ToInt32(result[3]);
                                        Convert.ToInt32(result[4]);
                                    }
                                    else if (Convert.ToInt32(result) < 0)
                                    {
                                        throw new FormatException();
                                    }
                            }
                            catch
                            {
                                MessageBox.Show("Неверный формат введённых данных, строка №" + (i + 1), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                for (int j = 0; j < standardsDGV.RowCount; j++)
                {
                    if (resultsDGV.Rows[i].Cells[0].Value.ToString() != standardsDGV.Rows[j].Cells[0].Value.ToString()) continue;
                    else if (standardsDGV.Rows[j].Cells[1].Value.ToString() == athleteGender || standardsDGV.Rows[j].Cells[1].Value.ToString() == "Для всех")
                    { 


                            beginner = standardsDGV.Rows[j].Cells[2].Value.ToString();
                            medium = standardsDGV.Rows[j].Cells[3].Value.ToString();
                            good = standardsDGV.Rows[j].Cells[4].Value.ToString();
                            elite = standardsDGV.Rows[j].Cells[5].Value.ToString();

                            try
                            {
                                if (result[2] == ':')
                                {

                                    b = ((Convert.ToInt32(beginner[0])-48) * 10 + (Convert.ToInt32(beginner[1])-48)) * 60 +
                                        ((Convert.ToInt32(beginner[3])-48) * 10 + (Convert.ToInt32(beginner[4])-48));
                                    m = ((Convert.ToInt32(medium[0]) - 48) * 10 + (Convert.ToInt32(medium[1]) - 48)) * 60 +
                                        ((Convert.ToInt32(medium[3]) - 48) * 10 + (Convert.ToInt32(medium[4]) - 48));
                                    g = ((Convert.ToInt32(good[0]) - 48) * 10 + (Convert.ToInt32(good[1]) - 48)) * 60 +
                                        ((Convert.ToInt32(good[3]) - 48) * 10 + (Convert.ToInt32(good[4]) - 48));
                                    el = ((Convert.ToInt32(elite[0]) - 48) * 10 + (Convert.ToInt32(elite[1]) - 48)) * 60 +
                                        ((Convert.ToInt32(elite[3]) - 48) * 10 + (Convert.ToInt32(elite[4]) - 48));
                                    r = ((Convert.ToInt32(result[0]) - 48) * 10 + (Convert.ToInt32(result[1]) - 48)) * 60 +
                                        ((Convert.ToInt32(result[3]) - 48) * 10 + (Convert.ToInt32(result[4]) - 48));


                                if (r <= el) level = "Элитный";
                                    else if (r <= g) level = "Продвинутый";
                                    else if (r <= m) level = "Средний";
                                    else if (r <= b) level = "Начальный";
                                    else level = "Низкий";

                                }
                                else throw new FormatException();
                            }
                            catch
                            {
                                try
                                {
                                    b = Convert.ToDouble(beginner);
                                    m = Convert.ToDouble(medium);
                                    g = Convert.ToDouble(good);
                                    el = Convert.ToDouble(elite);
                                    r = Convert.ToDouble(result);

                                    if (r >= el) level = "Элитный";
                                    else if (r >= g) level = "Продвинутый";
                                    else if (r >= m) level = "Средний";
                                    else if (r >= b) level = "Начальный";
                                    else level = "Низкий";
                                }
                                catch
                                {
                                    OleDbConnection connection1 = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
                                    DataSet dataSet1 = new DataSet();        // буфер для хранения таблиц данных
                                    OleDbDataAdapter dataSetAdapter1 = new OleDbDataAdapter();   // инструмент для работы с dataSet
                                    OleDbCommand command1 = new OleDbCommand();  // инструмент для sql запросов

                                    connection1.Open();     // открыть соединение с бд

                                        command1.Connection = connection1;
                                        dataSetAdapter1.SelectCommand = command1;

                                        command1.CommandText = "DELETE 1 FROM [Результаты] WHERE [Наименование] = '" + standardsDGV.Rows[j].Cells[0].Value.ToString() + "'";
                                        command1.ExecuteNonQuery();

                                        command1.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                                            "INNER JOIN (SELECT * FROM [Результаты] WHERE [Код атлета] = " + athleteCode + ") res ON [Атлет].[Код атлета] = res.[Код атлета]";
                                        dataSetAdapter1.Fill(dataSet1);
                                        resultsDGV.DataSource = dataSet1.Tables[0];

                                    connection1.Close();
                                    MessageBox.Show("Неправильный формат результата.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;

                                }
                            }

                            command.CommandText = "UPDATE [Результаты] SET [Результат] = '" + result + "', [Уровень атлета] = '" + level + "', " +
                            "[Дата фиксации] = '" + resultsDGV.Rows[i].Cells[3].Value.ToString() +
                                "' WHERE [Наименование] = '" + resultsDGV.Rows[i].Cells[0].Value.ToString() + "' AND [Код атлета] = " + athleteCode;
                            command.ExecuteNonQuery();       // выполнить запрос

                            break;
                        }
                }
            }
            command.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                "INNER JOIN (SELECT * FROM [Результаты] WHERE [Код атлета] = " + athleteCode + ") res ON [Атлет].[Код атлета] = res.[Код атлета]";
            dataSetAdapter.Fill(dataSet);
            resultsDGV.DataSource = dataSet.Tables[0];

            getNeedList();
            
            getTypeCountingTable("Бенчмарк");
            getTypeCountingTable("Комбинированное");
            getTypeCountingTable("Толкательное");
            getTypeCountingTable("Тяговое");
            getTypeCountingTable("Ноги");
            getTypeCountingTable("Пресс");
            getTypeCountingTable("Итоговый уровень");

            getModalityCountingTable("G");
            getModalityCountingTable("W");
            getModalityCountingTable("M");
            getModalityCountingTable("Итоговый уровень");

            DataSet dataSet2 = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet3 = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter2 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter3 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            dataSetAdapter2.SelectCommand = command;
            dataSetAdapter3.SelectCommand = command;

            command.CommandText = "SELECT [Бенчмарк], [Комбинированное], [Толкательное], [Тяговое], [Ноги], [Пресс], [Итоговый уровень] FROM [Уровень в движениях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter2.Fill(dataSet2);
            levelsDGV.DataSource = dataSet2.Tables[0];

            command.CommandText = "SELECT [G], [W], [M], [Итоговый уровень] FROM [Уровень в модальностях] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter3.Fill(dataSet3);
            modalitiesDGV.DataSource = dataSet3.Tables[0];

            connection.Close();

                this.chart1.Series["Комплексы"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[0].Value.ToString()));
                this.chart1.Series["Комбинированное"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[1].Value.ToString()));
                this.chart1.Series["Толкательное"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[2].Value.ToString()));
                this.chart1.Series["Тяговое"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[3].Value.ToString()));
                this.chart1.Series["Ноги"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[4].Value.ToString()));
                this.chart1.Series["Пресс"].Points[0].SetValueY(setLevelToInt(levelsDGV.Rows[0].Cells[5].Value.ToString()));
                this.chart2.Series["G"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[0].Value.ToString()));
                this.chart2.Series["W"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[1].Value.ToString()));
                this.chart2.Series["M"].Points[0].SetValueY(setLevelToInt(modalitiesDGV.Rows[0].Cells[2].Value.ToString()));

                this.chart1.Refresh();
                this.chart2.Refresh();

        }

        private void addAthleteButton_Click(object sender, EventArgs e)
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            DataSet dataSet1 = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbDataAdapter dataSetAdapter1 = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;
            dataSetAdapter1.SelectCommand = command;

            try
            {
                command.CommandText = "INSERT INTO [Атлет](Фамилия, Имя, Отчество, Пол, Возраст, [Стаж в кроссфите], [Итоговый спортивный стаж], Бекграунд, [Цели и примечания]) values('" +
                    fnameTextBox.Text + "', '" +
                    snameTextBox.Text + "', '" +
                    lnameTextBox.Text + "', '" +
                    genderComboBox.Text + "', " +
                    Convert.ToInt32(ageNUD.Value) + ", " +
                    Convert.ToInt32(cfExperienceNUD.Value) + ", " +
                    Convert.ToInt32(overallExperienceNUD.Value) + ", '" +
                    backgroundTextBox.Text + "', '" +
                    targetTextBox.Text + "')";
                command.ExecuteNonQuery();

                command.CommandText = "SELECT * FROM [Атлет]";
                dataSetAdapter.Fill(dataSet);
                athleteDataDGV.DataSource = dataSet.Tables[0];

                command.CommandText = "INSERT INTO [Уровень в движениях] values(" +
                    athleteDataDGV.Rows[athleteDataDGV.RowCount - 1].Cells[0].Value.ToString() + ", 'Начальный', 'Начальный','Начальный','Начальный','Начальный','Начальный','Начальный')";
                command.ExecuteNonQuery();

                command.CommandText = "INSERT INTO [Уровень в модальностях] values(" +
                    athleteDataDGV.Rows[athleteDataDGV.RowCount - 1].Cells[0].Value.ToString() + ", 'Начальный', 'Начальный','Начальный','Начальный')";
                command.ExecuteNonQuery();

                command.CommandText = "SELECT [Бенчмарк], [Комбинированное], [Толкательное], [Тяговое], [Ноги], [Пресс], [Итоговый уровень] FROM [Уровень в движениях] WHERE [Код атлета] = " + athleteCode;
                dataSetAdapter.Fill(dataSet);
                levelsDGV.DataSource = dataSet.Tables[0];
            }
            catch (OleDbException)
            {
                MessageBox.Show("Ошибка БД", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            connection.Close();
            MessageBox.Show("Атлет добавлен!");

            fnameTextBox.Text = "";
            snameTextBox.Text = "";
            lnameTextBox.Text = "";
            genderComboBox.Text = "";
            ageNUD.Value = 0;
            cfExperienceNUD.Value = 0;
            overallExperienceNUD.Value = 0;
            backgroundTextBox.Text = "";
            targetTextBox.Text = "";

        }

        private void deleteThisAthleteButton_Click(object sender, EventArgs e)
        {

            if (athleteDataDGV.RowCount == 0) return;

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            DialogResult dialogResult = MessageBox.Show("Удалить данного атлета?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.OK)
            {

                command.CommandText = "DELETE 1 FROM [Атлет] WHERE [Код атлета] = " + athleteDataDGV.CurrentRow.Cells[0].Value.ToString();
                command.ExecuteNonQuery();

                command.CommandText = "DELETE 1 FROM [Уровень в движениях] WHERE [Код атлета] = " + athleteDataDGV.CurrentRow.Cells[0].Value.ToString();
                command.ExecuteNonQuery();

                command.CommandText = "DELETE 1 FROM [Уровень в модальностях] WHERE [Код атлета] = " + athleteDataDGV.CurrentRow.Cells[0].Value.ToString();
                command.ExecuteNonQuery();

                command.CommandText = "SELECT * FROM [Атлет]";
                dataSetAdapter.Fill(dataSet);
                athleteDataDGV.DataSource = dataSet.Tables[0];

                MessageBox.Show("Атлет удалён!");

            }

            connection.Close();

            getNeedList();
        }

        private void addResultButton_Click(object sender, EventArgs e)
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;
            try
            {

                command.CommandText = "INSERT INTO [Результаты] values(" +
                    athleteCode + ", '" +
                    resultComboBox.Text + "', '" +
                    resultTextBox.Text + "', '" +
                    " " + "', '" +
                    DateTime.Now.ToShortDateString() + "')";
                command.ExecuteNonQuery();

                command.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                "INNER JOIN (SELECT * FROM [Результаты] WHERE [Код атлета] = " + athleteCode + ") res ON [Атлет].[Код атлета] = res.[Код атлета]";
                dataSetAdapter.Fill(dataSet);
                resultsDGV.DataSource = dataSet.Tables[0];
            }
            catch (OleDbException)
            {
                MessageBox.Show("Такой показатель уже есть в списке. ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch
            {
                MessageBox.Show("Неверный формат данных. ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            connection.Close();

            resultComboBox.Text = "";
            resultTextBox.Text = "";

            saveButton6_Click(sender, e);
            
        }

        private void deleteResultButton_Click(object sender, EventArgs e)
        {
            if (resultsDGV.RowCount == 0) return;

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            DialogResult dialogResult = MessageBox.Show("Удалить данного результат?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.OK)
            {

                command.CommandText = "DELETE FROM [Результаты] WHERE [Код атлета] = " + athleteCode + " AND [Наименование] = '" + resultsDGV.CurrentRow.Cells[0].Value.ToString() + "'";
                command.ExecuteNonQuery();

                command.CommandText = "SELECT [Наименование], [Результат], [Уровень атлета], [Дата фиксации] FROM [Атлет]" +
                "INNER JOIN [Результаты] ON [Атлет].[Код атлета] = [Результаты].[Код атлета] WHERE [Атлет].[Код атлета] = " + athleteCode;
                dataSetAdapter.Fill(dataSet);
                resultsDGV.DataSource = dataSet.Tables[0];

            }

            connection.Close();
        }

        private void listDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            wodDescrTextBox.Text = "";
            if (listDGV.CurrentRow.Cells[0].Value.ToString() == "Фрэн")
            {

                wodDescrTextBox.Text =
                    "\"Фрэн\"\n" +
                    "На время:\n" +
                    "21-15-9\n" +
                    "Трастеры 43/30 кг\n" +
                    "Подтягивания\n\n";

                getMoreWodInfo();

            }
            else if (listDGV.CurrentRow.Cells[0].Value.ToString() == "Грейс")
            {

                wodDescrTextBox.Text =
                    "\"Грейс\"\n" +
                    "На время:\n" +
                    "30 взятий и толчков 60/40 кг\n\n";

                getMoreWodInfo();

            }
            else getMoreWodInfo();
        }

        private void getNeedList()
        {

            listBox1.Items.Clear();
            for(int i = 0; i < listDGV.RowCount; i++)
            {

                if (Convert.ToBoolean(listDGV.Rows[i].Cells[4].Value))
                {
                    for (int j = 0; j < resultsDGV.RowCount; j++)
                    {
                        if (listDGV.Rows[i].Cells[0].Value.ToString() == resultsDGV.Rows[j].Cells[0].Value.ToString())
                        {
                            break;
                        }
                        else if (j == (resultsDGV.RowCount - 1))
                        {
                            for(int k = 0; k<standardsDGV.RowCount; k++)
                                if(standardsDGV.Rows[k].Cells[0].Value.ToString() == listDGV.Rows[i].Cells[0].Value.ToString() && (standardsDGV.Rows[k].Cells[1].Value.ToString() == athleteGender || standardsDGV.Rows[k].Cells[1].Value.ToString() == "Для всех"))
                                    listBox1.Items.Add(listDGV.Rows[i].Cells[0].Value.ToString());

                        }
                    }
                    if (resultsDGV.RowCount == 0)
                    {

                        for (int k = 0; k < standardsDGV.RowCount; k++)
                            if (standardsDGV.Rows[k].Cells[0].Value.ToString() == listDGV.Rows[i].Cells[0].Value.ToString() && standardsDGV.Rows[k].Cells[1].Value.ToString() == athleteGender)
                                listBox1.Items.Add(listDGV.Rows[i].Cells[0].Value.ToString());

                    }

                }
            }

        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainTabControl.SelectedTab.Text == "Выбор атлета")
                MessageBox.Show("1) Для выбора атлета необходимо нажать на запись с данными о нём;\n\n" +
                    "2) Чтобы добавить атлета, необходимо заполнить как минимум ФИО и пол, затем нажать кнопку \"Добавить атлета\";\n\n" +
                    "3) Чтобы удалить атлета, необходимо его выбрать, затем нажать кнопку \"Удалить отмеченного атлета\"(данные о нём навсегда будут потеряны!)."
                );
            else if (mainTabControl.SelectedTab.Text == "Показатели")
                MessageBox.Show("1) Чтобы посмотреть нормативы для выбранного показателя, можно нажать на запись с этим показателем, и после этого появится нормативы для него в специальном окошке."
                );
            else if (mainTabControl.SelectedTab.Text == "Результаты")
                MessageBox.Show("1) При переходе на данную вкладку, в таблице отображаются результаты только того атлета, ФИО которого указано в самом низу;\n\n" +
                    "2) Пока не будут присутствовать все необходимые результаты(пока окно \"Осталось необходимых показателей\" не окажется пустым), переход на вкладку \"Комплекс\" недоступен;\n\n" +
                    "3) Чтобы внести изменения в таблицу, сначала необходимо отредактировать таблицу с результатами, которая представлена во вкладке, затем нажать кнопку \"Принять изменения\";\n\n" +
                    "4) Чтобы удалить результат, необходимо нажать на запись с ним, затем нажать кнопку \"Удалить отмеченного результат\";\n\n" +
                    "5) В поле 'Результат' допустимы следующие форматы данных:\n" +
                    "    -только целые числа-это кол-во повторений или вес в показателе;\n" +
                    "    -текст в формате '99:59'-это время в показателе."
                );
            else if (mainTabControl.SelectedTab.Text == "Комплекс")
                MessageBox.Show("1) Чтобы сгенерировать комплекс, необходимо нажать кнопку \"Сгенерировать\";\n\n" +
                    "2) Чтобы откатить изменения, внесённые вами в комплекс, необходимо нажать кнопку \"Отменить\";\n\n" +
                    "3) Чтобы сохранить полученный комплекс, необходимо нажать кнопку \"Комплекс выполнен\";\n\n" +
                    "4) Регулирование дополнительных параметров комплекса доступно слева от окна для вывода комплекса."
                );
            else if (mainTabControl.SelectedTab.Text == "Выполненные комплексы")
                MessageBox.Show("1) Чтобы посмотреть описание выбранного комплекса, необходимо навести курсор на описание с этим комплексом.\n\n" +
                    "2) Чтобы удалить какой-либо комплекс, необходимо нажать на запись с ним, затем нажать кнопку \"Удалить отмеченный комплекс\"."
                );

        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            resultComboBox.Text = listBox1.SelectedItem.ToString();
        }
        private void getTypeCountingTable(string value)
        {

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            if(value != "Итоговый уровень")
                command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
                "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
                ") res ON[Показатели].[Наименование] = res.[Наименование] " +
                "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех') " + "AND [Показатели].[Тип движения] = '" + value +
                "'ORDER BY [Показатели].[Наименование] ASC";
            else
                command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
                "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
                ") res ON[Показатели].[Наименование] = res.[Наименование] " +
                "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех') "+
                "ORDER BY [Показатели].[Наименование] ASC";
            dataSetAdapter.Fill(dataSet);
            countingDGV.DataSource = dataSet.Tables[0];

            connection.Close();     // открыть соединение с бд

            getTypeLevel(value);

        }
        private void getModalityCountingTable(string value)
        {

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            if (value != "Итоговый уровень")
                command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
                "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
                ") res ON[Показатели].[Наименование] = res.[Наименование] " +
                "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех') " + "AND [Показатели].[Модальность] = '" + value +
                "'ORDER BY [Показатели].[Наименование] ASC";
            else
                command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
                "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
                ") res ON[Показатели].[Наименование] = res.[Наименование] " +
                "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех') " +
                "ORDER BY [Показатели].[Наименование] ASC";
            dataSetAdapter.Fill(dataSet);
            countingDGV.DataSource = dataSet.Tables[0];

            connection.Close();     // открыть соединение с бд

            getModalityLevel(value);

        }
        private void getTypeLevel(string value)
        {
            
            double avg = 0, sum = 0, count = 0;
            string typeLevel = "";

            for(int i = 0; i < countingDGV.RowCount; i++)
            {

                if (countingDGV.Rows[i].Cells[9].Value.ToString() == "") continue;
                count++;
                if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Начальный") sum += 1;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Средний") sum += 2;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Продвинутый") sum += 3;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Элитный") sum += 4;

            }

            avg = (sum / count);
            if (Math.Round(avg) == 4) typeLevel = "Элитный";
            else if (Math.Round(avg) == 3) typeLevel = "Продвинутый";
            else if (Math.Round(avg) == 2) typeLevel = "Средний";
            else if (Math.Round(avg) == 1) typeLevel = "Начальный";
            else if (Math.Round(avg) == 0) typeLevel = "Низкий";

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            command.CommandText = "UPDATE [Уровень в движениях] SET [" + value + "] = '" + 
                typeLevel + "' WHERE [Код атлета] = " + athleteCode;
            command.ExecuteNonQuery();

            

            connection.Close();     // открыть соединение с бд

        }
        private void getModalityLevel(string value)
        {

            double avg = 0, sum = 0, count = 0;
            string modalityLevel = "";

            for (int i = 0; i < countingDGV.RowCount; i++)
            {

                if (countingDGV.Rows[i].Cells[9].Value.ToString() == "") continue;
                count++;
                if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Начальный") sum += 1;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Средний") sum += 2;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Продвинутый") sum += 3;
                else if (countingDGV.Rows[i].Cells[9].Value.ToString() == "Элитный") sum += 4;

            }

            avg = (sum / count);
            if (Math.Round(avg) == 4) modalityLevel = "Элитный";
            else if (Math.Round(avg) == 3) modalityLevel = "Продвинутый";
            else if (Math.Round(avg) == 2) modalityLevel = "Средний";
            else if (Math.Round(avg) == 1) modalityLevel = "Начальный";
            else if (Math.Round(avg) == 0) modalityLevel = "Низкий";

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            command.CommandText = "UPDATE [Уровень в модальностях] SET [" + value + "] = '" +
                modalityLevel + "' WHERE [Код атлета] = " + athleteCode;
            command.ExecuteNonQuery();



            connection.Close();     // открыть соединение с бд

        }

        private void generateExercise()
        {
            weight = "";
            int sum = 0, sumEx = 0, random = 0, randomEx = 0;
            double result = 0;
            int j = 0;
            string[] Reps = new string[100];
            Reps = currentScheme.Split('-');

                                            for (j = 1; j < 6; j++)//подсчёт суммы для рандомизации типа упражнения
                                            switch (levelsDGV.Rows[0].Cells[j].Value.ToString())
                                            {

                                                case "Начальный":
                                                    sum += 4;
                                                    break;
                                                case "Средний":
                                                    sum += 3;
                                                    break;
                                                case "Продвинутый":
                                                    sum += 2;
                                                    break;
                                                case "Элитный":
                                                    sum += 1;
                                                    break;

                                            }
                            while (true)
                            {
                                        random = rand.Next(0, (sum + 1));

                                        j = 0;
                                        while (random > 0)//рандомизация типа упражнения
                                        {
                                            j++;
                                            switch (levelsDGV.Rows[0].Cells[j].Value.ToString())
                                            {

                                                case "Начальный":
                                                    random -= 4;
                                                    break;
                                                case "Средний":
                                                    random -= 3;
                                                    break;
                                                case "Продвинутый":
                                                    random -= 2;
                                                    break;
                                                case "Элитный":
                                                    random -= 1;
                                                    break;

                                            }

                                        }
                                        if (j == 0) j++;
                                        if (levelsDGV.Columns[j].HeaderText == previousExType) continue;
                                        else
                                        {

                                            previousExType = levelsDGV.Columns[j].HeaderText;
                                            break;

                                        }
                            }

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            command.CommandText = "SELECT [Показатели].[Наименование], [Показатели].[Тип движения], [Показатели].[Модальность], [Нормативы].[Норматив], [Нормативы].[Начальный], [Нормативы].[Средний], [Нормативы].[Продвинутый], [Нормативы].[Элитный], [Результаты].[Результат], [Результаты].[Уровень атлета] " +
            "FROM(Показатели LEFT JOIN Нормативы ON[Показатели].[Наименование] = [Нормативы].[Наименование])" + " LEFT JOIN(SELECT* FROM Результаты WHERE [Код атлета] = " + athleteCode +
            ") res ON[Показатели].[Наименование] = res.[Наименование] " +
            "WHERE ([Нормативы].[Норматив] = '" + athleteGender + "' OR [Нормативы].[Норматив] = 'Для всех') AND [Показатели].[Тип движения] = '" + levelsDGV.Columns[j].HeaderText + "'" +
            " ORDER BY [Показатели].[Наименование] ASC";
            dataSetAdapter.Fill(dataSet);
            countingDGV.DataSource = dataSet.Tables[0];//вызов таблицы генерации, в которой находятся упражнения только сгенерированного упражнения

            connection.Close();

            int k;


                                            for (k = 0; k < countingDGV.RowCount; k++)//подсчёт суммы для рандомизации самого упражнения
                                            {
                                                switch (countingDGV.Rows[k].Cells[9].Value.ToString())
                                                {

                                                    case "Начальный":
                                                        sumEx += 4;
                                                        break;
                                                    case "Средний":
                                                        sumEx += 3;
                                                        break;
                                                    case "Продвинутый":
                                                        sumEx += 2;
                                                        break;
                                                    case "Элитный":
                                                        sumEx += 1;
                                                        break;
                                                    case "":
                                                        switch (levelsDGV.Rows[0].Cells[j].Value.ToString())//вот такой уровень у тех упражнений, результат по которым не вписан
                                                        {

                                                            case "Начальный":
                                                                sumEx += 4;
                                                                break;
                                                            case "Средний":
                                                                sumEx += 3;
                                                                break;
                                                            case "Продвинутый":
                                                                sumEx += 2;
                                                                break;
                                                            case "Элитный":
                                                                sumEx += 1;
                                                                break;

                                                        }
                                                        break;

                                                }
                                            }
                                            int q = 0;


            while (true)//рандомизация упражнения
            {
                                            randomEx = rand.Next(0, (sumEx + 1));
                                            k = -1;
                                            while (randomEx > 0)
                                            {
                                                k++;
                                                switch (countingDGV.Rows[k].Cells[9].Value.ToString())//это уровень полученного упражнения, в k-той строчке
                                                {

                                                    case "Начальный":
                                                        randomEx -= 4;
                                                        break;
                                                    case "Средний":
                                                        randomEx -= 3;
                                                        break;
                                                    case "Продвинутый":
                                                        randomEx -= 2;
                                                        break;
                                                    case "Элитный":
                                                        randomEx -= 1;
                                                        break;
                                                    case "":
                                                        switch (levelsDGV.Rows[0].Cells[j].Value.ToString())//вот такой уровень у сгенерированного упражнения, результат по которому не вписан(равен уровню типа движения)
                                                        {

                                                            case "Начальный":
                                                                randomEx -= 4;
                                                                break;
                                                            case "Средний":
                                                                randomEx -= 3;
                                                                break;
                                                            case "Продвинутый":
                                                                randomEx -= 2;
                                                                break;
                                                            case "Элитный":
                                                                randomEx -= 1;
                                                                break;

                                                        }
                                                        break;
                                                }
                                            }
                                            if (k == -1) k = 0;///вот на этом этапе упражнение уже выбрано-это countingDGV.Rows[k].Cells[0].Value.ToString()
                                            bool exit = false;
                                            bool cont = false;
                                    
                                            for (q = 0; q < exercises.Count; q++)//проверка, не повторяется ли упражнение в комплексе
                                            {

                                                if (countingDGV.Rows[k].Cells[0].Value.ToString() == exercises[q])
                                                {

                                                    cont = true;
                                                    break;

                                                }
                                                else if (q == (exercises.Count - 1) )
                                                {
                                                    exercises.Add(countingDGV.Rows[k].Cells[0].Value.ToString());
                                                    exit = true;
                                                    q++;
                                                    break;

                                                }

                                            }
                                            if (exercises.Count == 0)
                                            {

                                                exercises.Add(countingDGV.Rows[k].Cells[0].Value.ToString());
                                                exit = true;

                                            }
                                            if (cont)
                                            {
                                                //exercises.RemoveAt(exercises.Count-1);
                                                continue;
                                            }



                                            if (countingDGV.Rows[k].Cells[2].Value.ToString() == "W")//масштабирование штанги
                                            {
                                                                    

                                                                    if (countingDGV.Rows[k].Cells[8].Value.ToString() == "")
                                                                    {

                                                                        for (int i = 1; i < 6; i++)
                                                                        {

                                                                            if (levelsDGV.Columns[i].HeaderText == countingDGV.Rows[k].Cells[1].Value.ToString())
                                                                            {

                                                                                switch (levelsDGV.Rows[0].Cells[i].Value.ToString())
                                                                                {

                                                                                    case "Начальный":
                                                                                        result = Convert.ToDouble(countingDGV.Rows[k].Cells[4].Value.ToString());
                                                                                        break;
                                                                                    case "Средний":
                                                                                        result = Convert.ToDouble(countingDGV.Rows[k].Cells[5].Value.ToString());
                                                                                        break;
                                                                                    case "Продвинутый":
                                                                                        result = Convert.ToDouble(countingDGV.Rows[k].Cells[6].Value.ToString());
                                                                                        break;
                                                                                    case "Элитный":
                                                                                        result = Convert.ToDouble(countingDGV.Rows[k].Cells[7].Value.ToString());
                                                                                        break;

                                                                                }

                                                                            }

                                                                        }

                                                                    }
                                                                    else result = Convert.ToDouble(countingDGV.Rows[k].Cells[8].Value.ToString());
                                                                    if (result == 0)
                                                                    {
                                                                        cont = true;
                                                                        break;
                                                                    }
                                                                    if (sumOfReps > (60*speed))
                                                                    {

                                                                        weight = (result * 0.35).ToString();

                                                                    }
                                                                    else scalingWeights(sumOfReps, result);
                                                                    exit = true;
                                                                    workoutTextBox.Text += currentScheme + "\n";
                                            }
                                            //масштабирование прочих упражнений
                                            else if (!countingDGV.Rows[k].Cells[0].Value.ToString().Contains("Бег") && !countingDGV.Rows[k].Cells[0].Value.ToString().Contains("Гребля 2 км"))
                                            {

                                                                    if (countingDGV.Rows[k].Cells[8].Value.ToString() == "")
                                                                    {
                                                                            for (int i = 1; i < 6; i++)
                                                                            {

                                                                                if (levelsDGV.Columns[i].HeaderText == countingDGV.Rows[k].Cells[1].Value.ToString())
                                                                                {

                                                                                    switch (levelsDGV.Rows[0].Cells[i].Value.ToString())
                                                                                    {

                                                                                        case "Начальный":
                                                                                            result = Convert.ToDouble(countingDGV.Rows[k].Cells[4].Value.ToString()) * speed;
                                                                                            break;
                                                                                        case "Средний":
                                                                                            result = Convert.ToDouble(countingDGV.Rows[k].Cells[5].Value.ToString()) * speed;
                                                                                            break;
                                                                                        case "Продвинутый":
                                                                                            result = Convert.ToDouble(countingDGV.Rows[k].Cells[6].Value.ToString()) * speed;
                                                                                            break;
                                                                                        case "Элитный":
                                                                                            result = Convert.ToDouble(countingDGV.Rows[k].Cells[7].Value.ToString()) * speed;
                                                                                            break;

                                                                                    }

                                                                                }

                                                                            }

                                                                    }
                                                                    else result = Convert.ToDouble(countingDGV.Rows[k].Cells[8].Value.ToString()) * speed;
                                                                    result *= (2 / exCount);//////////////////ПОРТИТСЯ ОФОРМЛЕНИЕ КОМПЛЕКСА
                                                                    if (result == 0)
                                                                    {
                                                                        continue;
                                                                    }
                                                                    if (countingDGV.Rows[k].Cells[0].Value.ToString().Contains("(мин)") 
                                                                        && 
                                                                    !countingDGV.Rows[k].Cells[0].Value.ToString().Contains("Ситап")
                                                                        &&
                                                                    !countingDGV.Rows[k].Cells[0].Value.ToString().Contains("байк")
                                                                        &&
                                                                    !countingDGV.Rows[k].Cells[0].Value.ToString().Contains("Воздушные приседания")
                                                                        &&
                                                                    !countingDGV.Rows[k].Cells[0].Value.ToString().Contains("урпи")) result *= 2;
                                                                    if (sumOfReps > result) {

                                                                        int div = Reps.Length;

                                                                        while (div > 0)
                                                                        {

                                                                            if((int)((int)result % div) != 0) workoutTextBox.Text += (int)(((int)result / div) + 1);
                                                                            else workoutTextBox.Text += (int)((int)result / div);
                                                                            result -= (int)((int)result / div);
                                                                            div--;
                                                                            if (result >= 1) workoutTextBox.Text += "-";

                                                                        }


                                                                        workoutTextBox.Text += "\n";

                                                                    }
                                                                    else
                                                                    for(int i = 10; i > 1; i--)
                                                                    {

                                                                            if (sumOfReps < Math.Round(result / i))
                                                                            {

                                                                                for (int a = 0; a < Reps.Length; a++)
                                                                                {
                                                                                    workoutTextBox.Text += (Convert.ToInt32(Reps[a]) * i).ToString();
                                                                                    if(a != (Reps.Length - 1))
                                                                                    {

                                                                                        workoutTextBox.Text += "-";

                                                                                    }

                                                                                }
                                                                                workoutTextBox.Text += "\n";
                                                                                break;

                                                                            }
                                                                            else if (i == 2)
                                                                            {

                                                                                workoutTextBox.Text += currentScheme + "\n";

                                                                            }

                                                                    }
                                                                exit = true;

                                        }
                                        else//определение расстояния, если выпали бег или гребля
                                        {
                                            
                                            continue;//exit = true; <-здесь должно быть это, но пока вот так

                                        }

                if (exit) break;
            }

            
            if(weight != "") workoutTextBox.Text += exercises[q].Replace("(мин)", "").Replace("3 км", "").Replace("2 км", "") + ", " + weight + " кг\n";
            else workoutTextBox.Text += exercises[q].Replace("(мин)", "").Replace("3 км", "").Replace("2 км", "") + "\n";

            savedWod = workoutTextBox.Text;

        }

        private void scalingWeights(int sumOfReps, double result)
        {

            double percentage = 0;

            for(int i = 0; i < scalingDGV.RowCount; i++)
            {

                if(sumOfReps >= Convert.ToInt32(scalingDGV.Rows[i].Cells[3].Value)*speed && sumOfReps <= Convert.ToInt32(scalingDGV.Rows[i].Cells[4].Value)*speed)
                {

                    do {

                        percentage = (double)rand.Next(Convert.ToInt32(scalingDGV.Rows[i].Cells[1].Value), (Convert.ToInt32(scalingDGV.Rows[i].Cells[2].Value) + 1)) / 100;
                        weight = (result * percentage).ToString();

                    } while ((percentage*100) % 5 != 0);
                    break;
                }

            }

        }

        private void обАвтореToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Данная программа предназначена для генерации комплексов на основе показателей атлетов\n" +
                "Автор: Чернышов Алексей\n" +
                "Email: alex9616@mail.ru");
        }

        private int setLevelToInt(string value)
        {

            switch (value)
            {
                
                case "Начальный":
                    return 1;
                case "Средний":
                    return 2;
                case "Продвинутый":
                    return 3;
                case "Элитный":
                    return 4;
                default:
                    return 0;

            }

        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            workoutTextBox.Text = savedWod;
        }

        private void wodIsDone_Click(object sender, EventArgs e)
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            command.CommandText = "INSERT INTO [Комплекс]([Код атлета], [Описание комплекса], [Результат], [Дата выполнения]) " +
                "values (" + athleteCode + ", '" + workoutTextBox.Text + "', '" + wodResultTextBox.Text + "', '" + DateTime.Now.ToString() + "')";
            command.ExecuteNonQuery();

            command.CommandText = "SELECT [Код записи], [Описание комплекса], [Результат], [Примечания], [Дата выполнения] FROM [Комплекс] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter.Fill(dataSet);
            doneWodsDGV.DataSource = dataSet.Tables[0];

            connection.Close();
        }

        private void saveWodChangesButton_Click(object sender, EventArgs e)
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            for (int i = 0; i < doneWodsDGV.RowCount; i++)
            {
                command.CommandText = "UPDATE [Комплекс] SET [Код атлета] = " + athleteCode + ", [Описание комплекса] = '" + doneWodsDGV.Rows[i].Cells[1].Value.ToString() +
                    "', [Результат] = '" + doneWodsDGV.Rows[i].Cells[2].Value.ToString() + "', [Примечания] = '" + doneWodsDGV.Rows[i].Cells[3].Value.ToString() + "', " +
                            "[Дата выполнения] = '" + doneWodsDGV.Rows[i].Cells[4].Value.ToString() +
                                "' WHERE [Код записи] = " + doneWodsDGV.Rows[i].Cells[0].Value.ToString();
                command.ExecuteNonQuery();       // выполнить запрос
            }

            command.CommandText = "SELECT [Код записи], [Описание комплекса], [Результат], [Примечания], [Дата выполнения] FROM [Комплекс] WHERE [Код атлета] = " + athleteCode;
            dataSetAdapter.Fill(dataSet);
            doneWodsDGV.DataSource = dataSet.Tables[0];

            connection.Close();
        }

        private void DeleteDoneWodButton_Click(object sender, EventArgs e)
        {
            if (doneWodsDGV.RowCount == 0) return;

            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=БД атлетов.mdb");
            DataSet dataSet = new DataSet();        // буфер для хранения таблиц данных
            OleDbDataAdapter dataSetAdapter = new OleDbDataAdapter();   // инструмент для работы с dataSet
            OleDbCommand command = new OleDbCommand();  // инструмент для sql запросов

            connection.Open();     // открыть соединение с бд

            command.Connection = connection;
            dataSetAdapter.SelectCommand = command;

            DialogResult dialogResult = MessageBox.Show("Удалить данный комплекс?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.OK)
            {

                command.CommandText = "DELETE 1 FROM [Комплекс] WHERE [Код записи] = " + doneWodsDGV.CurrentRow.Cells[0].Value.ToString();
                command.ExecuteNonQuery();

                command.CommandText = "SELECT [Код записи], [Описание комплекса], [Результат], [Примечания], [Дата выполнения] FROM [Комплекс] WHERE [Код атлета] = " + athleteCode;
                dataSetAdapter.Fill(dataSet);
                doneWodsDGV.DataSource = dataSet.Tables[0];

                MessageBox.Show("Комплекс удалён!");

            }

            connection.Close();
        }
    }
}