﻿namespace AutoWOD
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.StripLine stripLine3 = new System.Windows.Forms.DataVisualization.Charting.StripLine();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.StripLine stripLine4 = new System.Windows.Forms.DataVisualization.Charting.StripLine();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(5D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(6D, 0D);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.действияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обАвтореToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modalitiesDGV = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.wodGeneration = new System.Windows.Forms.TabPage();
            this.wodResultTextBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.charRB1 = new System.Windows.Forms.RadioButton();
            this.charRB6 = new System.Windows.Forms.RadioButton();
            this.charRB5 = new System.Windows.Forms.RadioButton();
            this.charRB4 = new System.Windows.Forms.RadioButton();
            this.charRB3 = new System.Windows.Forms.RadioButton();
            this.charRB2 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.wodIsDone = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.undoButton = new System.Windows.Forms.Button();
            this.workoutTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.generateButton = new System.Windows.Forms.Button();
            this.ExAndT = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.wodDescrTextBox = new System.Windows.Forms.RichTextBox();
            this.listDGV = new System.Windows.Forms.DataGridView();
            this.athleteData = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.overallExperienceNUD = new System.Windows.Forms.NumericUpDown();
            this.cfExperienceNUD = new System.Windows.Forms.NumericUpDown();
            this.ageNUD = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.targetTextBox = new System.Windows.Forms.TextBox();
            this.backgroundTextBox = new System.Windows.Forms.TextBox();
            this.lnameTextBox = new System.Windows.Forms.TextBox();
            this.snameTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.deleteThisAthleteButton = new System.Windows.Forms.Button();
            this.athleteDataDGV = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.addAthleteButton = new System.Windows.Forms.Button();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.results = new System.Windows.Forms.TabPage();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.docButton = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.resultComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.addResultButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.deleteResultButton = new System.Windows.Forms.Button();
            this.saveButton6 = new System.Windows.Forms.Button();
            this.resultsDGV = new System.Windows.Forms.DataGridView();
            this.doneWods = new System.Windows.Forms.TabPage();
            this.saveWodChangesButton = new System.Windows.Forms.Button();
            this.DeleteDoneWodButton = new System.Windows.Forms.Button();
            this.doneWodsDGV = new System.Windows.Forms.DataGridView();
            this.standards = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.standardsDGV = new System.Windows.Forms.DataGridView();
            this.scalingDGV = new System.Windows.Forms.DataGridView();
            this.levelsDGV = new System.Windows.Forms.DataGridView();
            this.countingDGV = new System.Windows.Forms.DataGridView();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modalitiesDGV)).BeginInit();
            this.wodGeneration.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ExAndT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDGV)).BeginInit();
            this.athleteData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overallExperienceNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cfExperienceNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ageNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.athleteDataDGV)).BeginInit();
            this.mainTabControl.SuspendLayout();
            this.results.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDGV)).BeginInit();
            this.doneWods.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doneWodsDGV)).BeginInit();
            this.standards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.standardsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scalingDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countingDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.действияToolStripMenuItem,
            this.помощьToolStripMenuItem,
            this.обАвтореToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1077, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // действияToolStripMenuItem
            // 
            this.действияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.действияToolStripMenuItem.Name = "действияToolStripMenuItem";
            this.действияToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.действияToolStripMenuItem.Text = "Действия";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(181, 20);
            this.помощьToolStripMenuItem.Text = "Помощь по текущей вкладке";
            this.помощьToolStripMenuItem.Click += new System.EventHandler(this.помощьToolStripMenuItem_Click);
            // 
            // обАвтореToolStripMenuItem1
            // 
            this.обАвтореToolStripMenuItem1.Name = "обАвтореToolStripMenuItem1";
            this.обАвтореToolStripMenuItem1.Size = new System.Drawing.Size(94, 20);
            this.обАвтореToolStripMenuItem1.Text = "О программе";
            this.обАвтореToolStripMenuItem1.Click += new System.EventHandler(this.обАвтореToolStripMenuItem1_Click);
            // 
            // modalitiesDGV
            // 
            this.modalitiesDGV.AllowUserToAddRows = false;
            this.modalitiesDGV.AllowUserToDeleteRows = false;
            this.modalitiesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.modalitiesDGV.Location = new System.Drawing.Point(357, 703);
            this.modalitiesDGV.Name = "modalitiesDGV";
            this.modalitiesDGV.Size = new System.Drawing.Size(194, 114);
            this.modalitiesDGV.TabIndex = 8;
            this.modalitiesDGV.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(466, 680);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "ФИО атлета:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(573, 680);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "не выбран";
            // 
            // wodGeneration
            // 
            this.wodGeneration.Controls.Add(this.wodResultTextBox);
            this.wodGeneration.Controls.Add(this.label29);
            this.wodGeneration.Controls.Add(this.panel3);
            this.wodGeneration.Controls.Add(this.panel2);
            this.wodGeneration.Controls.Add(this.panel1);
            this.wodGeneration.Controls.Add(this.label8);
            this.wodGeneration.Controls.Add(this.label28);
            this.wodGeneration.Controls.Add(this.label26);
            this.wodGeneration.Controls.Add(this.label25);
            this.wodGeneration.Controls.Add(this.comboBox4);
            this.wodGeneration.Controls.Add(this.wodIsDone);
            this.wodGeneration.Controls.Add(this.label14);
            this.wodGeneration.Controls.Add(this.undoButton);
            this.wodGeneration.Controls.Add(this.workoutTextBox);
            this.wodGeneration.Controls.Add(this.label2);
            this.wodGeneration.Controls.Add(this.generateButton);
            this.wodGeneration.Location = new System.Drawing.Point(4, 27);
            this.wodGeneration.Name = "wodGeneration";
            this.wodGeneration.Padding = new System.Windows.Forms.Padding(3);
            this.wodGeneration.Size = new System.Drawing.Size(1064, 619);
            this.wodGeneration.TabIndex = 1;
            this.wodGeneration.Text = "Комплекс";
            this.wodGeneration.UseVisualStyleBackColor = true;
            // 
            // wodResultTextBox
            // 
            this.wodResultTextBox.Location = new System.Drawing.Point(853, 589);
            this.wodResultTextBox.Name = "wodResultTextBox";
            this.wodResultTextBox.Size = new System.Drawing.Size(73, 24);
            this.wodResultTextBox.TabIndex = 76;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(763, 591);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(84, 18);
            this.label29.TabIndex = 75;
            this.label29.Text = "Результат:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.charRB1);
            this.panel3.Controls.Add(this.charRB6);
            this.panel3.Controls.Add(this.charRB5);
            this.panel3.Controls.Add(this.charRB4);
            this.panel3.Controls.Add(this.charRB3);
            this.panel3.Controls.Add(this.charRB2);
            this.panel3.Location = new System.Drawing.Point(46, 377);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(184, 174);
            this.panel3.TabIndex = 74;
            // 
            // charRB1
            // 
            this.charRB1.AutoSize = true;
            this.charRB1.Checked = true;
            this.charRB1.Location = new System.Drawing.Point(26, 6);
            this.charRB1.Name = "charRB1";
            this.charRB1.Size = new System.Drawing.Size(89, 22);
            this.charRB1.TabIndex = 74;
            this.charRB1.TabStop = true;
            this.charRB1.Text = "Неважно";
            this.charRB1.UseVisualStyleBackColor = true;
            // 
            // charRB6
            // 
            this.charRB6.AutoSize = true;
            this.charRB6.Location = new System.Drawing.Point(28, 146);
            this.charRB6.Name = "charRB6";
            this.charRB6.Size = new System.Drawing.Size(154, 22);
            this.charRB6.TabIndex = 57;
            this.charRB6.TabStop = true;
            this.charRB6.Text = "Очень медленный";
            this.charRB6.UseVisualStyleBackColor = true;
            // 
            // charRB5
            // 
            this.charRB5.AutoSize = true;
            this.charRB5.Location = new System.Drawing.Point(26, 118);
            this.charRB5.Name = "charRB5";
            this.charRB5.Size = new System.Drawing.Size(108, 22);
            this.charRB5.TabIndex = 56;
            this.charRB5.TabStop = true;
            this.charRB5.Text = "Медленный";
            this.charRB5.UseVisualStyleBackColor = true;
            // 
            // charRB4
            // 
            this.charRB4.AutoSize = true;
            this.charRB4.Location = new System.Drawing.Point(28, 90);
            this.charRB4.Name = "charRB4";
            this.charRB4.Size = new System.Drawing.Size(106, 22);
            this.charRB4.TabIndex = 55;
            this.charRB4.TabStop = true;
            this.charRB4.Text = "Умеренный";
            this.charRB4.UseVisualStyleBackColor = true;
            // 
            // charRB3
            // 
            this.charRB3.AutoSize = true;
            this.charRB3.Location = new System.Drawing.Point(28, 62);
            this.charRB3.Name = "charRB3";
            this.charRB3.Size = new System.Drawing.Size(89, 22);
            this.charRB3.TabIndex = 54;
            this.charRB3.TabStop = true;
            this.charRB3.Text = "Быстрый";
            this.charRB3.UseVisualStyleBackColor = true;
            // 
            // charRB2
            // 
            this.charRB2.AutoSize = true;
            this.charRB2.Location = new System.Drawing.Point(28, 34);
            this.charRB2.Name = "charRB2";
            this.charRB2.Size = new System.Drawing.Size(76, 22);
            this.charRB2.TabIndex = 53;
            this.charRB2.TabStop = true;
            this.charRB2.Text = "Спринт";
            this.charRB2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton19);
            this.panel2.Controls.Add(this.radioButton11);
            this.panel2.Controls.Add(this.radioButton6);
            this.panel2.Controls.Add(this.radioButton7);
            this.panel2.Controls.Add(this.radioButton8);
            this.panel2.Controls.Add(this.radioButton9);
            this.panel2.Controls.Add(this.radioButton10);
            this.panel2.Location = new System.Drawing.Point(63, 124);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 210);
            this.panel2.TabIndex = 73;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Checked = true;
            this.radioButton19.Location = new System.Drawing.Point(11, 7);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(89, 22);
            this.radioButton19.TabIndex = 73;
            this.radioButton19.TabStop = true;
            this.radioButton19.Text = "Неважно";
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Enabled = false;
            this.radioButton11.Location = new System.Drawing.Point(11, 120);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(130, 22);
            this.radioButton11.TabIndex = 64;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "Только штанга";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Enabled = false;
            this.radioButton6.Location = new System.Drawing.Point(11, 148);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(136, 22);
            this.radioButton6.TabIndex = 63;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Только гантели";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Enabled = false;
            this.radioButton7.Location = new System.Drawing.Point(11, 176);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(112, 22);
            this.radioButton7.TabIndex = 62;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Только гири";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Enabled = false;
            this.radioButton8.Location = new System.Drawing.Point(11, 92);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(78, 22);
            this.radioButton8.TabIndex = 61;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "Кардио";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Enabled = false;
            this.radioButton9.Location = new System.Drawing.Point(11, 62);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(156, 22);
            this.radioButton9.TabIndex = 60;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Без оборудования";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Enabled = false;
            this.radioButton10.Location = new System.Drawing.Point(11, 35);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(139, 22);
            this.radioButton10.TabIndex = 59;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "Тяжёлая штанга";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton18);
            this.panel1.Controls.Add(this.radioButton12);
            this.panel1.Controls.Add(this.radioButton13);
            this.panel1.Controls.Add(this.radioButton14);
            this.panel1.Controls.Add(this.radioButton15);
            this.panel1.Controls.Add(this.radioButton16);
            this.panel1.Controls.Add(this.radioButton17);
            this.panel1.Location = new System.Drawing.Point(291, 126);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 208);
            this.panel1.TabIndex = 72;
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.Checked = true;
            this.radioButton18.Location = new System.Drawing.Point(3, 5);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(89, 22);
            this.radioButton18.TabIndex = 72;
            this.radioButton18.TabStop = true;
            this.radioButton18.Text = "Неважно";
            this.radioButton18.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Enabled = false;
            this.radioButton12.Location = new System.Drawing.Point(3, 174);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(241, 22);
            this.radioButton12.TabIndex = 71;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Каждую минуту с увеличением";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Enabled = false;
            this.radioButton13.Location = new System.Drawing.Point(3, 146);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(215, 22);
            this.radioButton13.TabIndex = 70;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "На время + каждую минуту";
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Enabled = false;
            this.radioButton14.Location = new System.Drawing.Point(3, 61);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(181, 22);
            this.radioButton14.TabIndex = 69;
            this.radioButton14.TabStop = true;
            this.radioButton14.Text = "На время не разбивая";
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Enabled = false;
            this.radioButton15.Location = new System.Drawing.Point(3, 89);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(247, 22);
            this.radioButton15.TabIndex = 68;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "Как можно больше повторений";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.Enabled = false;
            this.radioButton16.Location = new System.Drawing.Point(3, 117);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(134, 22);
            this.radioButton16.TabIndex = 67;
            this.radioButton16.TabStop = true;
            this.radioButton16.Text = "Каждую минуту";
            this.radioButton16.UseVisualStyleBackColor = true;
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.Enabled = false;
            this.radioButton17.Location = new System.Drawing.Point(3, 33);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(92, 22);
            this.radioButton17.TabIndex = 66;
            this.radioButton17.TabStop = true;
            this.radioButton17.Text = "На время";
            this.radioButton17.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(249, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 20);
            this.label8.TabIndex = 65;
            this.label8.Text = "Тип комплекса:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(37, 93);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(113, 20);
            this.label28.TabIndex = 58;
            this.label28.Text = "Особенности:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(34, 349);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(171, 20);
            this.label26.TabIndex = 52;
            this.label26.Text = "Характер комплекса:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(249, 349);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(237, 20);
            this.label25.TabIndex = 51;
            this.label25.Text = "Присутствующее упражнение:";
            // 
            // comboBox4
            // 
            this.comboBox4.AllowDrop = true;
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.Enabled = false;
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBox4.Items.AddRange(new object[] {
            "100 бурпи",
            "CF Total 1(сумма)",
            "CF Total 2(сумма)",
            "DT",
            "FGB",
            "Ассаулт байк, калории(мин)",
            "Бег 3 км",
            "Бул",
            "Бурпи(мин)",
            "Взятие и толчок",
            "Взятие и толчок гантели 20 кг(мин)",
            "Взятие на грудь в сед",
            "Взятие на грудь в стойку",
            "Взятие на грудь с виса в сед",
            "Взятие на грудь с виса в стойку",
            "Воздушное приседание(мин)",
            "Выход на кольцах",
            "Выход на турнике",
            "Гребля 2 км",
            "Гребля, калории(мин)",
            "Грейс",
            "Двойные прыжки на скакалке",
            "Диана",
            "Донни",
            "Жим стоя",
            "Жимовой швунг",
            "Колени к груди",
            "Махи гири 16 кг",
            "Махи гири 24 кг",
            "Мёрф",
            "Носки к перекладине",
            "Нэнси",
            "Отжимание",
            "Отжимание в стойке на руках",
            "Пистолетик(мин)",
            "Подтягивание",
            "Подтягивание с весом 12 кг",
            "Подтягивание с весом 20 кг",
            "Подъём по канату 10 м",
            "Подъём по канату 10 м без ног",
            "Приседание с гантелями на плечах 10 кг",
            "Приседание с гантелями на плечах 15 кг",
            "Приседание с гантелями на плечах 20 кг",
            "Приседание с гантелями на плечах 30 кг",
            "Приседание со штангой на груди",
            "Приседание со штангой на спине",
            "Приседание со штангой над головой",
            "Рывок в сед",
            "Рывок в стойку",
            "Семь",
            "Ситап на GHD(мин)",
            "Ситап(мин)",
            "Становая тяга",
            "Строгое отжимание в стойке на руках",
            "Строгое подтягивание",
            "Толчковый швунг",
            "Трастер",
            "Фрэн",
            "Элизабет"});
            this.comboBox4.Location = new System.Drawing.Point(287, 384);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(239, 26);
            this.comboBox4.TabIndex = 50;
            // 
            // wodIsDone
            // 
            this.wodIsDone.Location = new System.Drawing.Point(932, 560);
            this.wodIsDone.Name = "wodIsDone";
            this.wodIsDone.Size = new System.Drawing.Size(126, 53);
            this.wodIsDone.TabIndex = 14;
            this.wodIsDone.Text = "Комплекс выполнен";
            this.wodIsDone.UseVisualStyleBackColor = true;
            this.wodIsDone.Click += new System.EventHandler(this.wodIsDone_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(566, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 20);
            this.label14.TabIndex = 13;
            this.label14.Text = "Комплекс:";
            // 
            // undoButton
            // 
            this.undoButton.Location = new System.Drawing.Point(629, 505);
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(127, 29);
            this.undoButton.TabIndex = 5;
            this.undoButton.Text = "Отменить изменения";
            this.undoButton.UseVisualStyleBackColor = true;
            this.undoButton.Click += new System.EventHandler(this.undoButton_Click);
            // 
            // workoutTextBox
            // 
            this.workoutTextBox.Font = new System.Drawing.Font("Segoe Script", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.workoutTextBox.Location = new System.Drawing.Point(570, 69);
            this.workoutTextBox.Name = "workoutTextBox";
            this.workoutTextBox.Size = new System.Drawing.Size(488, 430);
            this.workoutTextBox.TabIndex = 4;
            this.workoutTextBox.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(14, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дополнительные параметры:";
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(932, 505);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(126, 29);
            this.generateButton.TabIndex = 1;
            this.generateButton.Text = "Сгенерировать";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // ExAndT
            // 
            this.ExAndT.Controls.Add(this.textBox2);
            this.ExAndT.Controls.Add(this.comboBox3);
            this.ExAndT.Controls.Add(this.label15);
            this.ExAndT.Controls.Add(this.label3);
            this.ExAndT.Controls.Add(this.wodDescrTextBox);
            this.ExAndT.Controls.Add(this.listDGV);
            this.ExAndT.Location = new System.Drawing.Point(4, 27);
            this.ExAndT.Name = "ExAndT";
            this.ExAndT.Size = new System.Drawing.Size(1064, 619);
            this.ExAndT.TabIndex = 8;
            this.ExAndT.Text = "Показатели";
            this.ExAndT.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(715, 263);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(142, 24);
            this.textBox2.TabIndex = 31;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(573, 263);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(137, 26);
            this.comboBox3.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(490, 268);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 18);
            this.label15.TabIndex = 27;
            this.label15.Text = "Поиск по:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(770, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 26;
            this.label3.Text = "Описание:";
            // 
            // wodDescrTextBox
            // 
            this.wodDescrTextBox.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wodDescrTextBox.Location = new System.Drawing.Point(773, 316);
            this.wodDescrTextBox.Name = "wodDescrTextBox";
            this.wodDescrTextBox.ReadOnly = true;
            this.wodDescrTextBox.Size = new System.Drawing.Size(283, 294);
            this.wodDescrTextBox.TabIndex = 25;
            this.wodDescrTextBox.Text = "";
            // 
            // listDGV
            // 
            this.listDGV.AllowUserToAddRows = false;
            this.listDGV.AllowUserToDeleteRows = false;
            this.listDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.listDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.listDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listDGV.Location = new System.Drawing.Point(8, 12);
            this.listDGV.Name = "listDGV";
            this.listDGV.ReadOnly = true;
            this.listDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.listDGV.Size = new System.Drawing.Size(849, 245);
            this.listDGV.TabIndex = 24;
            this.listDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listDGV_CellClick);
            // 
            // athleteData
            // 
            this.athleteData.Controls.Add(this.button5);
            this.athleteData.Controls.Add(this.button4);
            this.athleteData.Controls.Add(this.checkBox1);
            this.athleteData.Controls.Add(this.textBox3);
            this.athleteData.Controls.Add(this.comboBox1);
            this.athleteData.Controls.Add(this.label24);
            this.athleteData.Controls.Add(this.overallExperienceNUD);
            this.athleteData.Controls.Add(this.cfExperienceNUD);
            this.athleteData.Controls.Add(this.ageNUD);
            this.athleteData.Controls.Add(this.label20);
            this.athleteData.Controls.Add(this.label19);
            this.athleteData.Controls.Add(this.targetTextBox);
            this.athleteData.Controls.Add(this.backgroundTextBox);
            this.athleteData.Controls.Add(this.lnameTextBox);
            this.athleteData.Controls.Add(this.snameTextBox);
            this.athleteData.Controls.Add(this.fnameTextBox);
            this.athleteData.Controls.Add(this.label21);
            this.athleteData.Controls.Add(this.label16);
            this.athleteData.Controls.Add(this.label17);
            this.athleteData.Controls.Add(this.label18);
            this.athleteData.Controls.Add(this.label10);
            this.athleteData.Controls.Add(this.label9);
            this.athleteData.Controls.Add(this.genderComboBox);
            this.athleteData.Controls.Add(this.deleteThisAthleteButton);
            this.athleteData.Controls.Add(this.athleteDataDGV);
            this.athleteData.Controls.Add(this.label7);
            this.athleteData.Controls.Add(this.label6);
            this.athleteData.Controls.Add(this.addAthleteButton);
            this.athleteData.Location = new System.Drawing.Point(4, 27);
            this.athleteData.Name = "athleteData";
            this.athleteData.Padding = new System.Windows.Forms.Padding(3);
            this.athleteData.Size = new System.Drawing.Size(1064, 619);
            this.athleteData.TabIndex = 0;
            this.athleteData.Text = "Выбор атлета";
            this.athleteData.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 328);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(233, 39);
            this.button5.TabIndex = 48;
            this.button5.Text = "Сделать атлета неактивным";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 281);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(233, 39);
            this.button4.TabIndex = 47;
            this.button4.Text = "Сделать атлета активным";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(355, 222);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(217, 22);
            this.checkBox1.TabIndex = 46;
            this.checkBox1.Text = "Показать только активных";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(912, 211);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(142, 24);
            this.textBox3.TabIndex = 45;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(770, 211);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(137, 26);
            this.comboBox1.TabIndex = 44;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(687, 216);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 18);
            this.label24.TabIndex = 43;
            this.label24.Text = "Поиск по:";
            // 
            // overallExperienceNUD
            // 
            this.overallExperienceNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.overallExperienceNUD.Location = new System.Drawing.Point(957, 451);
            this.overallExperienceNUD.Name = "overallExperienceNUD";
            this.overallExperienceNUD.Size = new System.Drawing.Size(101, 24);
            this.overallExperienceNUD.TabIndex = 42;
            // 
            // cfExperienceNUD
            // 
            this.cfExperienceNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cfExperienceNUD.Location = new System.Drawing.Point(957, 421);
            this.cfExperienceNUD.Name = "cfExperienceNUD";
            this.cfExperienceNUD.Size = new System.Drawing.Size(101, 24);
            this.cfExperienceNUD.TabIndex = 41;
            // 
            // ageNUD
            // 
            this.ageNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ageNUD.Location = new System.Drawing.Point(957, 391);
            this.ageNUD.Name = "ageNUD";
            this.ageNUD.Size = new System.Drawing.Size(101, 24);
            this.ageNUD.TabIndex = 40;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(892, 246);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(166, 20);
            this.label20.TabIndex = 39;
            this.label20.Text = "Добавление атлета:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(867, 483);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 18);
            this.label19.TabIndex = 38;
            this.label19.Text = "Бекграунд:";
            // 
            // targetTextBox
            // 
            this.targetTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.targetTextBox.Location = new System.Drawing.Point(958, 510);
            this.targetTextBox.Name = "targetTextBox";
            this.targetTextBox.Size = new System.Drawing.Size(100, 24);
            this.targetTextBox.TabIndex = 35;
            // 
            // backgroundTextBox
            // 
            this.backgroundTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backgroundTextBox.Location = new System.Drawing.Point(958, 480);
            this.backgroundTextBox.Name = "backgroundTextBox";
            this.backgroundTextBox.Size = new System.Drawing.Size(100, 24);
            this.backgroundTextBox.TabIndex = 33;
            // 
            // lnameTextBox
            // 
            this.lnameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lnameTextBox.Location = new System.Drawing.Point(957, 329);
            this.lnameTextBox.Name = "lnameTextBox";
            this.lnameTextBox.Size = new System.Drawing.Size(100, 24);
            this.lnameTextBox.TabIndex = 24;
            // 
            // snameTextBox
            // 
            this.snameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.snameTextBox.Location = new System.Drawing.Point(957, 299);
            this.snameTextBox.Name = "snameTextBox";
            this.snameTextBox.Size = new System.Drawing.Size(100, 24);
            this.snameTextBox.TabIndex = 23;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fnameTextBox.Location = new System.Drawing.Point(957, 269);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(100, 24);
            this.fnameTextBox.TabIndex = 13;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(804, 513);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(147, 18);
            this.label21.TabIndex = 34;
            this.label21.Text = "Цели и примечания:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(881, 393);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 18);
            this.label16.TabIndex = 32;
            this.label16.Text = "Возраст:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(745, 453);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(206, 18);
            this.label17.TabIndex = 31;
            this.label17.Text = "Итоговый спортивный стаж:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(809, 423);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(142, 18);
            this.label18.TabIndex = 28;
            this.label18.Text = "Стаж в кроссфите:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(874, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 18);
            this.label10.TabIndex = 26;
            this.label10.Text = "Фамилия:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(875, 332);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 18);
            this.label9.TabIndex = 25;
            this.label9.Text = "Отчество:";
            // 
            // genderComboBox
            // 
            this.genderComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Items.AddRange(new object[] {
            "Мужской",
            "Женский"});
            this.genderComboBox.Location = new System.Drawing.Point(958, 359);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(99, 26);
            this.genderComboBox.TabIndex = 22;
            // 
            // deleteThisAthleteButton
            // 
            this.deleteThisAthleteButton.Location = new System.Drawing.Point(8, 213);
            this.deleteThisAthleteButton.Name = "deleteThisAthleteButton";
            this.deleteThisAthleteButton.Size = new System.Drawing.Size(233, 39);
            this.deleteThisAthleteButton.TabIndex = 21;
            this.deleteThisAthleteButton.Text = "Удалить отмеченного атлета";
            this.deleteThisAthleteButton.UseVisualStyleBackColor = true;
            this.deleteThisAthleteButton.Click += new System.EventHandler(this.deleteThisAthleteButton_Click);
            // 
            // athleteDataDGV
            // 
            this.athleteDataDGV.AllowUserToAddRows = false;
            this.athleteDataDGV.AllowUserToDeleteRows = false;
            this.athleteDataDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.athleteDataDGV.Location = new System.Drawing.Point(8, 6);
            this.athleteDataDGV.Name = "athleteDataDGV";
            this.athleteDataDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.athleteDataDGV.Size = new System.Drawing.Size(1050, 199);
            this.athleteDataDGV.TabIndex = 19;
            this.athleteDataDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.athleteDataDGV_CellClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(913, 362);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "Пол:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(909, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 18);
            this.label6.TabIndex = 16;
            this.label6.Text = "Имя:";
            // 
            // addAthleteButton
            // 
            this.addAthleteButton.Location = new System.Drawing.Point(917, 558);
            this.addAthleteButton.Name = "addAthleteButton";
            this.addAthleteButton.Size = new System.Drawing.Size(141, 34);
            this.addAthleteButton.TabIndex = 15;
            this.addAthleteButton.Text = "Добавить атлета";
            this.addAthleteButton.UseVisualStyleBackColor = true;
            this.addAthleteButton.Click += new System.EventHandler(this.addAthleteButton_Click);
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.athleteData);
            this.mainTabControl.Controls.Add(this.results);
            this.mainTabControl.Controls.Add(this.wodGeneration);
            this.mainTabControl.Controls.Add(this.doneWods);
            this.mainTabControl.Controls.Add(this.ExAndT);
            this.mainTabControl.Controls.Add(this.standards);
            this.mainTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mainTabControl.Location = new System.Drawing.Point(0, 27);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1072, 650);
            this.mainTabControl.TabIndex = 3;
            this.mainTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.mainTabControl_Selected);
            // 
            // results
            // 
            this.results.Controls.Add(this.label33);
            this.results.Controls.Add(this.label32);
            this.results.Controls.Add(this.label31);
            this.results.Controls.Add(this.label30);
            this.results.Controls.Add(this.chart2);
            this.results.Controls.Add(this.chart1);
            this.results.Controls.Add(this.docButton);
            this.results.Controls.Add(this.label27);
            this.results.Controls.Add(this.label23);
            this.results.Controls.Add(this.listBox1);
            this.results.Controls.Add(this.label22);
            this.results.Controls.Add(this.resultTextBox);
            this.results.Controls.Add(this.resultComboBox);
            this.results.Controls.Add(this.label12);
            this.results.Controls.Add(this.label13);
            this.results.Controls.Add(this.addResultButton);
            this.results.Controls.Add(this.label1);
            this.results.Controls.Add(this.deleteResultButton);
            this.results.Controls.Add(this.saveButton6);
            this.results.Controls.Add(this.resultsDGV);
            this.results.Location = new System.Drawing.Point(4, 27);
            this.results.Name = "results";
            this.results.Size = new System.Drawing.Size(1064, 619);
            this.results.TabIndex = 10;
            this.results.Text = "Результаты";
            this.results.UseVisualStyleBackColor = true;
            // 
            // chart2
            // 
            chartArea3.AxisX.StripLines.Add(stripLine3);
            chartArea3.AxisX.Title = "Уровень модальностей";
            chartArea3.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(523, 412);
            this.chart2.Name = "chart2";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "G";
            dataPoint10.Label = "";
            series10.Points.Add(dataPoint10);
            series11.ChartArea = "ChartArea1";
            series11.Legend = "Legend1";
            series11.Name = "W";
            series11.Points.Add(dataPoint11);
            series12.ChartArea = "ChartArea1";
            series12.Legend = "Legend1";
            series12.Name = "M";
            series12.Points.Add(dataPoint12);
            this.chart2.Series.Add(series10);
            this.chart2.Series.Add(series11);
            this.chart2.Series.Add(series12);
            this.chart2.Size = new System.Drawing.Size(434, 151);
            this.chart2.TabIndex = 60;
            this.chart2.Text = "chart2";
            // 
            // chart1
            // 
            chartArea4.AxisX.StripLines.Add(stripLine4);
            chartArea4.AxisX.Title = "Уровень движений";
            chartArea4.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(523, 255);
            this.chart1.Name = "chart1";
            series13.ChartArea = "ChartArea1";
            series13.Legend = "Legend1";
            series13.Name = "Комплексы";
            dataPoint13.Label = "";
            series13.Points.Add(dataPoint13);
            series14.ChartArea = "ChartArea1";
            series14.Legend = "Legend1";
            series14.Name = "Комбинированное";
            series14.Points.Add(dataPoint14);
            series15.ChartArea = "ChartArea1";
            series15.Legend = "Legend1";
            series15.Name = "Толкательное";
            series15.Points.Add(dataPoint15);
            series16.ChartArea = "ChartArea1";
            series16.Legend = "Legend1";
            series16.Name = "Тяговое";
            series16.Points.Add(dataPoint16);
            series17.ChartArea = "ChartArea1";
            series17.Color = System.Drawing.Color.Lime;
            series17.Legend = "Legend1";
            series17.Name = "Ноги";
            series17.Points.Add(dataPoint17);
            series17.ShadowColor = System.Drawing.Color.Silver;
            series18.ChartArea = "ChartArea1";
            series18.Color = System.Drawing.Color.DarkOrchid;
            series18.Legend = "Legend1";
            series18.Name = "Пресс";
            dataPoint18.Color = System.Drawing.Color.DarkOrchid;
            series18.Points.Add(dataPoint18);
            this.chart1.Series.Add(series13);
            this.chart1.Series.Add(series14);
            this.chart1.Series.Add(series15);
            this.chart1.Series.Add(series16);
            this.chart1.Series.Add(series17);
            this.chart1.Series.Add(series18);
            this.chart1.Size = new System.Drawing.Size(525, 151);
            this.chart1.TabIndex = 59;
            this.chart1.Text = "chart1";
            // 
            // docButton
            // 
            this.docButton.Location = new System.Drawing.Point(457, 143);
            this.docButton.Name = "docButton";
            this.docButton.Size = new System.Drawing.Size(129, 67);
            this.docButton.TabIndex = 58;
            this.docButton.Text = "Сформировать отчёт о результатах";
            this.docButton.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(848, 103);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(125, 18);
            this.label27.TabIndex = 57;
            this.label27.Text = "*число или 99:59";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(4, 271);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(294, 20);
            this.label23.TabIndex = 54;
            this.label23.Text = "Осталось необходимых показателей:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 18;
            this.listBox1.Location = new System.Drawing.Point(8, 294);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(290, 202);
            this.listBox1.TabIndex = 53;
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(454, 329);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 52;
            this.label22.Text = "Ось Y:";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(734, 76);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.Size = new System.Drawing.Size(73, 24);
            this.resultTextBox.TabIndex = 50;
            // 
            // resultComboBox
            // 
            this.resultComboBox.AllowDrop = true;
            this.resultComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resultComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultComboBox.FormattingEnabled = true;
            this.resultComboBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.resultComboBox.Items.AddRange(new object[] {
            "100 бурпи",
            "CF Total 1(сумма)",
            "CF Total 2(сумма)",
            "DT",
            "FGB",
            "Ассаулт байк, калории(мин)",
            "Бег 3 км",
            "Бул",
            "Бурпи(мин)",
            "Взятие и толчок",
            "Взятие и толчок гантели 20 кг(мин)",
            "Взятие на грудь в сед",
            "Взятие на грудь в стойку",
            "Взятие на грудь с виса в сед",
            "Взятие на грудь с виса в стойку",
            "Воздушное приседание(мин)",
            "Выход на кольцах",
            "Выход на турнике",
            "Гребля 2 км",
            "Гребля, калории(мин)",
            "Грейс",
            "Двойные прыжки на скакалке",
            "Диана",
            "Донни",
            "Жим стоя",
            "Жимовой швунг",
            "Колени к груди",
            "Махи гири 16 кг",
            "Махи гири 24 кг",
            "Мёрф",
            "Носки к перекладине",
            "Нэнси",
            "Отжимание",
            "Отжимание в стойке на руках",
            "Пистолетик(мин)",
            "Подтягивание",
            "Подтягивание с весом 12 кг",
            "Подтягивание с весом 20 кг",
            "Подъём по канату 10 м",
            "Подъём по канату 10 м без ног",
            "Приседание с гантелями на плечах 10 кг",
            "Приседание с гантелями на плечах 15 кг",
            "Приседание с гантелями на плечах 20 кг",
            "Приседание с гантелями на плечах 30 кг",
            "Приседание со штангой на груди",
            "Приседание со штангой на спине",
            "Приседание со штангой над головой",
            "Рывок в сед",
            "Рывок в стойку",
            "Семь",
            "Ситап на GHD(мин)",
            "Ситап(мин)",
            "Становая тяга",
            "Строгое отжимание в стойке на руках",
            "Строгое подтягивание",
            "Толчковый швунг",
            "Трастер",
            "Фрэн",
            "Элизабет"});
            this.resultComboBox.Location = new System.Drawing.Point(734, 44);
            this.resultComboBox.Name = "resultComboBox";
            this.resultComboBox.Size = new System.Drawing.Size(239, 26);
            this.resultComboBox.TabIndex = 49;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(613, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 18);
            this.label12.TabIndex = 48;
            this.label12.Text = "Наименование:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(644, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 18);
            this.label13.TabIndex = 47;
            this.label13.Text = "Результат:";
            // 
            // addResultButton
            // 
            this.addResultButton.Location = new System.Drawing.Point(813, 76);
            this.addResultButton.Name = "addResultButton";
            this.addResultButton.Size = new System.Drawing.Size(160, 24);
            this.addResultButton.TabIndex = 46;
            this.addResultButton.Text = "Добавить результат";
            this.addResultButton.UseVisualStyleBackColor = true;
            this.addResultButton.Click += new System.EventHandler(this.addResultButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(691, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 20);
            this.label1.TabIndex = 43;
            this.label1.Text = "Добавление результатов:";
            // 
            // deleteResultButton
            // 
            this.deleteResultButton.Location = new System.Drawing.Point(457, 71);
            this.deleteResultButton.Name = "deleteResultButton";
            this.deleteResultButton.Size = new System.Drawing.Size(129, 66);
            this.deleteResultButton.TabIndex = 45;
            this.deleteResultButton.Text = "Удалить отмеченный результат";
            this.deleteResultButton.UseVisualStyleBackColor = true;
            this.deleteResultButton.Click += new System.EventHandler(this.deleteResultButton_Click);
            // 
            // saveButton6
            // 
            this.saveButton6.Location = new System.Drawing.Point(457, 12);
            this.saveButton6.Name = "saveButton6";
            this.saveButton6.Size = new System.Drawing.Size(129, 53);
            this.saveButton6.TabIndex = 44;
            this.saveButton6.Text = "Принять изменения";
            this.saveButton6.UseVisualStyleBackColor = true;
            this.saveButton6.Click += new System.EventHandler(this.saveButton6_Click);
            // 
            // resultsDGV
            // 
            this.resultsDGV.AllowUserToAddRows = false;
            this.resultsDGV.AllowUserToDeleteRows = false;
            this.resultsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsDGV.Location = new System.Drawing.Point(8, 12);
            this.resultsDGV.Name = "resultsDGV";
            this.resultsDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultsDGV.Size = new System.Drawing.Size(442, 240);
            this.resultsDGV.TabIndex = 42;
            // 
            // doneWods
            // 
            this.doneWods.Controls.Add(this.saveWodChangesButton);
            this.doneWods.Controls.Add(this.DeleteDoneWodButton);
            this.doneWods.Controls.Add(this.doneWodsDGV);
            this.doneWods.Location = new System.Drawing.Point(4, 27);
            this.doneWods.Name = "doneWods";
            this.doneWods.Size = new System.Drawing.Size(1064, 619);
            this.doneWods.TabIndex = 11;
            this.doneWods.Text = "Выполненные комплексы";
            this.doneWods.UseVisualStyleBackColor = true;
            // 
            // saveWodChangesButton
            // 
            this.saveWodChangesButton.Location = new System.Drawing.Point(264, 271);
            this.saveWodChangesButton.Name = "saveWodChangesButton";
            this.saveWodChangesButton.Size = new System.Drawing.Size(181, 39);
            this.saveWodChangesButton.TabIndex = 46;
            this.saveWodChangesButton.Text = "Принять изменения";
            this.saveWodChangesButton.UseVisualStyleBackColor = true;
            this.saveWodChangesButton.Click += new System.EventHandler(this.saveWodChangesButton_Click);
            // 
            // DeleteDoneWodButton
            // 
            this.DeleteDoneWodButton.Location = new System.Drawing.Point(10, 271);
            this.DeleteDoneWodButton.Name = "DeleteDoneWodButton";
            this.DeleteDoneWodButton.Size = new System.Drawing.Size(248, 39);
            this.DeleteDoneWodButton.TabIndex = 45;
            this.DeleteDoneWodButton.Text = "Удалить отмеченный комплекс";
            this.DeleteDoneWodButton.UseVisualStyleBackColor = true;
            this.DeleteDoneWodButton.Click += new System.EventHandler(this.DeleteDoneWodButton_Click);
            // 
            // doneWodsDGV
            // 
            this.doneWodsDGV.AllowUserToAddRows = false;
            this.doneWodsDGV.AllowUserToDeleteRows = false;
            this.doneWodsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doneWodsDGV.Location = new System.Drawing.Point(10, 25);
            this.doneWodsDGV.Name = "doneWodsDGV";
            this.doneWodsDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.doneWodsDGV.Size = new System.Drawing.Size(536, 240);
            this.doneWodsDGV.TabIndex = 43;
            // 
            // standards
            // 
            this.standards.Controls.Add(this.textBox1);
            this.standards.Controls.Add(this.comboBox2);
            this.standards.Controls.Add(this.label11);
            this.standards.Controls.Add(this.standardsDGV);
            this.standards.Location = new System.Drawing.Point(4, 27);
            this.standards.Name = "standards";
            this.standards.Size = new System.Drawing.Size(1064, 619);
            this.standards.TabIndex = 9;
            this.standards.Text = "Нормативы";
            this.standards.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(743, 248);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(142, 24);
            this.textBox1.TabIndex = 30;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(599, 248);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(137, 26);
            this.comboBox2.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(516, 251);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 18);
            this.label11.TabIndex = 28;
            this.label11.Text = "Поиск по:";
            // 
            // standardsDGV
            // 
            this.standardsDGV.AllowUserToAddRows = false;
            this.standardsDGV.AllowUserToDeleteRows = false;
            this.standardsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.standardsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.standardsDGV.Location = new System.Drawing.Point(8, 12);
            this.standardsDGV.Name = "standardsDGV";
            this.standardsDGV.ReadOnly = true;
            this.standardsDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.standardsDGV.Size = new System.Drawing.Size(877, 230);
            this.standardsDGV.TabIndex = 27;
            // 
            // scalingDGV
            // 
            this.scalingDGV.AllowUserToAddRows = false;
            this.scalingDGV.AllowUserToDeleteRows = false;
            this.scalingDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scalingDGV.Location = new System.Drawing.Point(782, 703);
            this.scalingDGV.Name = "scalingDGV";
            this.scalingDGV.Size = new System.Drawing.Size(121, 72);
            this.scalingDGV.TabIndex = 56;
            this.scalingDGV.Visible = false;
            // 
            // levelsDGV
            // 
            this.levelsDGV.AllowUserToAddRows = false;
            this.levelsDGV.AllowUserToDeleteRows = false;
            this.levelsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.levelsDGV.Location = new System.Drawing.Point(577, 703);
            this.levelsDGV.Name = "levelsDGV";
            this.levelsDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.levelsDGV.Size = new System.Drawing.Size(182, 72);
            this.levelsDGV.TabIndex = 51;
            this.levelsDGV.Visible = false;
            // 
            // countingDGV
            // 
            this.countingDGV.AllowUserToAddRows = false;
            this.countingDGV.AllowUserToDeleteRows = false;
            this.countingDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.countingDGV.Location = new System.Drawing.Point(68, 703);
            this.countingDGV.Name = "countingDGV";
            this.countingDGV.Size = new System.Drawing.Size(258, 130);
            this.countingDGV.TabIndex = 55;
            this.countingDGV.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(454, 355);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 13);
            this.label30.TabIndex = 61;
            this.label30.Text = "1-Начальный";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(454, 380);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(59, 13);
            this.label31.TabIndex = 62;
            this.label31.Text = "2-Средний";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(454, 409);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(84, 13);
            this.label32.TabIndex = 63;
            this.label32.Text = "3-Продвинутый";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(454, 436);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(60, 13);
            this.label33.TabIndex = 64;
            this.label33.Text = "4-Элитный";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 700);
            this.Controls.Add(this.modalitiesDGV);
            this.Controls.Add(this.countingDGV);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.scalingDGV);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.levelsDGV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Составление комплексов";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modalitiesDGV)).EndInit();
            this.wodGeneration.ResumeLayout(false);
            this.wodGeneration.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ExAndT.ResumeLayout(false);
            this.ExAndT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDGV)).EndInit();
            this.athleteData.ResumeLayout(false);
            this.athleteData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overallExperienceNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cfExperienceNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ageNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.athleteDataDGV)).EndInit();
            this.mainTabControl.ResumeLayout(false);
            this.results.ResumeLayout(false);
            this.results.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDGV)).EndInit();
            this.doneWods.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.doneWodsDGV)).EndInit();
            this.standards.ResumeLayout(false);
            this.standards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.standardsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scalingDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countingDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem действияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обАвтореToolStripMenuItem1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView modalitiesDGV;
        private System.Windows.Forms.TabPage wodGeneration;
        private System.Windows.Forms.Button wodIsDone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button undoButton;
        private System.Windows.Forms.RichTextBox workoutTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TabPage ExAndT;
        private System.Windows.Forms.TabPage athleteData;
        private System.Windows.Forms.NumericUpDown overallExperienceNUD;
        private System.Windows.Forms.NumericUpDown cfExperienceNUD;
        private System.Windows.Forms.NumericUpDown ageNUD;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox targetTextBox;
        private System.Windows.Forms.TextBox backgroundTextBox;
        private System.Windows.Forms.TextBox lnameTextBox;
        private System.Windows.Forms.TextBox snameTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.Button deleteThisAthleteButton;
        private System.Windows.Forms.DataGridView athleteDataDGV;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button addAthleteButton;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage standards;
        private System.Windows.Forms.TabPage results;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox wodDescrTextBox;
        private System.Windows.Forms.DataGridView listDGV;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView standardsDGV;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.ComboBox resultComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button addResultButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button deleteResultButton;
        private System.Windows.Forms.Button saveButton6;
        private System.Windows.Forms.DataGridView resultsDGV;
        private System.Windows.Forms.TabPage doneWods;
        private System.Windows.Forms.DataGridView doneWodsDGV;
        private System.Windows.Forms.DataGridView levelsDGV;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridView countingDGV;
        private System.Windows.Forms.DataGridView scalingDGV;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Button DeleteDoneWodButton;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button docButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton charRB1;
        private System.Windows.Forms.RadioButton charRB6;
        private System.Windows.Forms.RadioButton charRB5;
        private System.Windows.Forms.RadioButton charRB4;
        private System.Windows.Forms.RadioButton charRB3;
        private System.Windows.Forms.RadioButton charRB2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button saveWodChangesButton;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox wodResultTextBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
    }
}

